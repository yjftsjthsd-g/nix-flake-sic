{ pkgs ? import <nixpkgs> {} }:

with pkgs;

stdenv.mkDerivation rec {
    name = "sic";
    version = "1.2";
    src = fetchurl {
        url = "https://dl.suckless.org/tools/sic-${version}.tar.gz";
        hash = {
            "1.2" = "sha256-rAf5BZleE7osQ5EtegNfu+eKYo17ocJW9MoTcvtWUYU=";
        }.${version} or "sha256-0000000000000000000000000000000000000000000=";
    };
    buildInputs = [
        pkgs.xlibs.libX11
            pkgs.xlibs.libXft
    ];
    #unpackPhase =
    #    ''
    #    mkdir sic
    #    cd sic
    #    tar xzf $src
    #    '';
    buildPhase = "make PREFIX=$out";
    installPhase =
        ''
        make install PREFIX=$out
        '';

    meta = {
        description = "simple irc client";
        homepage = "https://tools.suckless.org/sic/";
        license = "MIT";
    };
}
