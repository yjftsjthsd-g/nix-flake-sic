# nix-flake-sic

A nix flake for suckless sic (IRC client).


## Use

On a flake-enabled nix system (see https://nixos.wiki/wiki/Flakes), either:

Clone this repo and
```
nix build
./result/bin/sic
```
(Note that this is an example from the upstream repo but with all images
stripped out, which makes some slides nonsensical but was good enough for me to
verify that the program actually worked.)

Or just run directly with
```
nix run gitlab:yjftsjthsd-g/nix-flake-sic -- -h server -n username
```


## License

The contents of this repo is under the ISC license (see LICENSE file), and the
sic progam is under the MIT license.

